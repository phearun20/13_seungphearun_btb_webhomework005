import React from "react";
import categoryIcon from "../Images/Images/category_icon.png";
import cube from "../Images/Images/cube.png";
import list from "../Images/Images/list.png";
import message from "../Images/Images/messenger.png";
import success from "../Images/Images/success.png";
import security from "../Images/Images/security.png";
import user from "../Images/Images/users.png";
import christ from "../Images/Images/christina.jpg";
import lachlan from "../Images/Images/lachlan.jpg";
import raamin from "../Images/Images/raamin.jpg";
import plus from "../Images/Images/plus.png";

function FirstComponent() {
  return (
    <div className="">
      <div className="mt-8 px-16 ">
        <img src={categoryIcon} />
      </div>
      <div className="mt-16 px-16 ">
        <img src={cube} />
      </div>
      <div className="mt-8 px-16">
        <img src={list} />
      </div>
      <div className="mt-8 px-16">
        <img src={message} />
      </div>
      <div className="mt-8 px-16">
        <img src={list} />
      </div>

      <div className="mt-16 px-16">
        <img src={success} />
      </div>
      <div className="mt-8 px-16">  
        <img src={security} />
      </div>
      <div className="mt-8 px-16">
        <img src={user} />
      </div>

      <div className="pl-14 mt-16  ">
        <img className="w-10 h-10 mt-10 rounded-full" src={christ} />
        <img className="w-10 h-10 mt-10 rounded-full" src={raamin} />
        <img className="w-10 h-10 mt-10 rounded-full" src={lachlan} />
      </div> 
      <div className="mt-8 px-16">
        <img src={plus} />
      </div>
    </div>
  );
}

export default FirstComponent;
