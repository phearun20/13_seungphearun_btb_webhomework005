import React, { useState } from 'react'
import FirstComponent from './FirstComponent'
import Home from './FirstComponent'
import SecondComponent from './SecondComponent'
import Second from './SecondComponent'
import ThirdComponent from './ThirdComponent'

export default function home({data, setData}) {
  // console.log(data);

  return (
    <div className='grid grid-cols-10 w-screen h-screen'>
            <div className='bg-slate-200'>
                <FirstComponent/>
            </div>

            <div className='col-span-6 bg-white'>
            <SecondComponent data={data} setData={setData}/>
            </div>
                
            <div className='col-span-3' id='Third'>
              <ThirdComponent/>
            </div>
                   
    </div>
  )
}
