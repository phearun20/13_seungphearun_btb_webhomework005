import React, { useState } from "react";
import FormComponent from "./FormComponent";

function SecondComponent({ data, setData }) {
  const [show, setShow] = useState(false);
  const [read, setReadMore] = useState([]);
  console.log(data);

  const onButtonChange = (id) => {
    // console.log(id)
    data.map((items) => {
      if (items.id === id) {
        if (items.status === "beach") {
          items.status = "mountain";
        } else if (items.status === "mountain") {
          items.status = "forest";
        } else {
          items.status = "beach";
        }
      }
    });

    setData([...data]);
  };

  return (
    <div>
      <div>
        <div className="flex justify-between pt-20">
          <h1 className="text-4xl px-10">Good Evening Team!</h1>
          <button
            onClick={() => setShow(!show)}
            type="button"
            class="text-white bg-gradient-to-br from-green-400 to-blue-600 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-green-200 dark:focus:ring-green-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
          >
            Add new trip
          </button>
          {show ? (
            <FormComponent
              show={show}
              setShow={setShow}
              data={data}
              setData={setData}
            />
          ) : null}
        </div>
      </div>

      <div class="m-2 flex flex-wrap">
        {data.map((item) => (
          <div
            key={item.id}
            class="card w-72 bg-gray-500 rounded-lg text-primary-content m-1"
          >
            <div className="card-body m-2 text-white">
              <h1 className="card-title text-2xl">{item.title}</h1>
              <br />
              <p className="line-clamp-3">{item.description}</p>
              {/* text-sm line-clamp-3 */}
              <br />
              <p>People Going</p>
              <h1 className="text-2xl">{item.peopleGoing}</h1>

              <div className=" flex  justify-center items-center card-actions ">
                <div className="flex space-x-2">
                  <button
                    className={`${
                      item.status === "beach"
                        ? "bg-blue-300 "
                        : item.status === "mountain"
                        ? "bg-gray-400 "
                        : "bg-green-500 "
                    } btn text-white w-24`}
                    onClick={() => onButtonChange(item.id)}
                  >
                    {item.status}
                  </button>
                  {/* <button
                    type="button"
                    className="py-2.5 px-5 mr-2 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 "
                  >
                    Read Details
                  </button>         */}

                  <label
                    onClick={() => setReadMore(item)}
                    htmlFor="my-modal"
                    className="btn"
                  >
                    Read Details
                  </label>

                  <input
                    type="checkbox"
                    id="my-modal"
                    className="modal-toggle"
                  />
                  <div className="modal">
                    <div className="modal-box">
                      <h3 className="font-bold text-lg text-black">
                        {read.title}
                      </h3>
                      <p className="py-4 text-black">{read.description}</p>
                      <br />
                      <p className="text-black">
                        Around {read.peopleGoing} people going there
                      </p>
                      <div className="modal-action">
                        <label htmlFor="my-modal" className="btn">
                          Close
                        </label>
                      </div>
                    </div>
                  </div>

                  {/* <label
                    onClick={() => setReadMore(item)}
                    htmlFor="my-modal-6"
                    className="w-[125px]  bg-red-500 font-bold p-2.5 rounded-lg text-center"
                  >
                    Read details
                  </label>
                  <input
                    type="checkbox"
                    id="my-modal-6"
                    className="modal-toggle"
                  />
                  <div className="modal modal-bottom sm:modal-middle">
                    <div className="modal-box">
                      <h3 className="font-bold text-lg text-black">
                        {read.title}
                      </h3>
                      <p className="py-4 text-black">{read.description}</p>
                      <br />
                      <p className="text-black">
                        Around {read.peopleGoing} people going there
                      </p>

                      <div className="modal-action">
                        <label htmlFor="my-modal-6" className="btn">
                          Close
                        </label> */}
                  {/* </div>
                    </div>
                  </div> */}

                </div>
              </div>
            </div>
          </div>
        ))}
        <form>
          <div
            id="staticModal"
            data-modal-backdrop="static"
            tabindex="-1"
            aria-hidden="true"
            class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full"
          >
            <div class="relative w-full h-full max-w-2xl md:h-auto">
              <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                  <h3 class="text-xl font-semibold text-gray-900 dark:text-white"></h3>
                  <p></p>
                  <p></p>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default SecondComponent;
