import React from "react";
import noti from "../Images/Images/notification.png";
import comment from "../Images/Images/comment.png";
import raamin from "../Images/Images/raamin.jpg";
import lachlan from "../Images/Images/lachlan.jpg";
import christina from "../Images/Images/christina.jpg";
import noname from "../Images/Images/nonamesontheway.jpg";

function ThirdComponent() {
  return (
    <div>
      <div className="flex justify-end m-10">
        <div className="w-8 m-2">
          <img src={noti} />
        </div>
        <div className="w-8 m-2">
          <img src={comment} />
        </div>
        <div className="m-2">
          <img className="w-10 h-10 rounded-full" src={christina} />
        </div>
      </div>

      <div className="flex justify-end m-10">
        <button
          type="button"
          class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
        >
          My amazing trip
        </button>
      </div>
      <p className="text-3xl m-4 text-white">
        I like laying down on the sand and looking at the moon.
      </p>
      <br />
      <br />
      <br />
      <p className="m-4 text-white">27 people going on this trip.</p>
      <div className="pl-4  flex   ">
        <img className="w-12 h-12 mx-4 rounded-full" src={christina} />
        <img className="w-12 h-12 mx-4  rounded-full" src={raamin} />
        <img className="w-12 h-12 mx-4 rounded-full" src={lachlan} />
        <img className="w-12 h-12 mx-4 rounded-full" src={noname} /> 
        <p className="w-12 h-12 mx-4 rounded-full bg-yellow-200 text-black p-2">+23</p>
      </div>
      
    </div>
  );
}

export default ThirdComponent;
