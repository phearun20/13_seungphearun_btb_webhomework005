/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx}"],
  theme: {
    

    extend: {},
  },

  plugins: [require("flowbite/plugin")],

  plugins: [require("daisyui"),require("@tailwindcss/line-clamp")],

};
